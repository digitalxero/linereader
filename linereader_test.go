package linereader_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
	"gitlab.com/digitalxero/linereader"
)

func TestLineReader_Lines(t *testing.T) {
	var (
		sr       linereader.LineReader
		expected = 3
		found    = 0
	)
	sr = linereader.NewLineReader().WithIOReader(strings.NewReader(`line: 0
line: 1
line: 2`)).Run()

EOF:
	for {
		select {
		case _, open := <-sr.Lines():
			if !open {
				break EOF
			}
			found++
		case <-sr.Context().Done():
			break EOF
		}
	}
	assert.Equal(t, expected, found)
}

func TestLineReader_LinesMax(t *testing.T) {
	var (
		sr       linereader.LineReader
		expected = 1
		found    = 0
	)
	sr = linereader.NewLineReader().WithIOReader(strings.NewReader(`line: 0
line: 1
line: 2`)).WithNumLines(1).Run()

EOF:
	for {
		select {
		case _, open := <-sr.Lines():
			if !open {
				break EOF
			}
			found++
		case <-sr.Context().Done():
			break EOF
		}
	}
	assert.Equal(t, expected, found)
}

func TestLineReader_LinesOffset(t *testing.T) {
	var (
		sr       linereader.LineReader
		expected = []string{"line: 1", "line: 2"}
		found    []string
	)
	sr = linereader.NewLineReader().WithIOReader(strings.NewReader(`line: 0
line: 1
line: 2`)).WithOffset(1).Run()

EOF:
	for {
		select {
		case line, open := <-sr.Lines():
			if !open {
				break EOF
			}
			found = append(found, line)
		case <-sr.Context().Done():
			break EOF
		}
	}
	assert.Equal(t, expected, found)
}

func TestLineReader_LinesOffsetMax(t *testing.T) {
	var (
		sr       linereader.LineReader
		expected = []string{"line: 2"}
		found    []string
	)
	sr = linereader.NewLineReader().WithIOReader(strings.NewReader(`line: 0
line: 1
line: 2
line: 3
line: 4
line: 5`)).WithOffset(2).WithNumLines(1).Run()

EOF:
	for {
		select {
		case line, open := <-sr.Lines():
			if !open {
				break EOF
			}
			found = append(found, line)
		case <-sr.Context().Done():
			break EOF
		}
	}
	assert.Equal(t, expected, found)
}
