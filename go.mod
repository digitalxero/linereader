// +heroku goVersion go1.15
module gitlab.com/digitalxero/linereader

go 1.15

require github.com/stretchr/testify v1.6.1
