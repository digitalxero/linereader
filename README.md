# linereader
This is a library to read lines from an `io.Reader` and return them on a channel

## Usage
```go
package main

import (
	"strings"

    "gitlab.com/digitalxero/linereader"
)


func main() {
    lr := linereader.NewLineReader().WithIOReader(strings.NewReader(`line: 0
line: 1
line: 2`)).Run()
    for {
        select {
        case line, open := <-lr.Lines():
            if !open {
                return
            }
            println(line)
        case <-lr.Context().Done():
            return
        }
    }
}
```