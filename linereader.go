package linereader

import (
	"bufio"
	"context"
	"io"
	"io/ioutil"
	"strings"
	"sync/atomic"
	"time"
)

type LineReader interface {
	Lines() <-chan string
	Context() context.Context
	WithTimeout(duration time.Duration) LineReader
	WithOffset(uint) LineReader
	WithNumLines(uint) LineReader
	WithIOReader(io.Reader) LineReader
	WithIOReadCloser(io.ReadCloser) LineReader
	Run() LineReader
}

func NewLineReader() LineReader {
	return NewLineReaderWithContext(context.Background())
}

func NewLineReaderWithContext(ctx context.Context) LineReader {
	return &lineReader{
		ctx:     ctx,
		timeout: time.Millisecond,
	}
}

// lineReader takes an io.Reader and pushes the lines out onto the channel.
type lineReader struct {
	reader   io.ReadCloser
	timeout  time.Duration
	offset   uint
	numLines uint
	ctx      context.Context
	// This will be closed when there are no more lines (io.EOF).
	lines   chan string
	started uint32
}

func (r *lineReader) Lines() <-chan string {
	return r.lines
}

func (r *lineReader) Context() context.Context {
	return r.ctx
}

func (r *lineReader) WithTimeout(duration time.Duration) LineReader {
	if r.started == 1 {
		return r
	}
	r.timeout = duration

	return r
}

func (r *lineReader) WithOffset(offset uint) LineReader {
	if r.started == 1 {
		return r
	}
	r.offset = offset

	return r
}

func (r *lineReader) WithNumLines(numLines uint) LineReader {
	if r.started == 1 {
		return r
	}
	r.numLines = numLines

	return r
}

func (r *lineReader) WithIOReader(reader io.Reader) LineReader {
	if r.started == 1 {
		return r
	}
	r.reader = ioutil.NopCloser(reader)

	return r
}

func (r *lineReader) WithIOReadCloser(reader io.ReadCloser) LineReader {
	if r.started == 1 {
		return r
	}
	r.reader = reader

	return r
}

func (r *lineReader) Run() LineReader {
	if !atomic.CompareAndSwapUint32(&r.started, 0, 1) {
		return r
	}
	r.lines = make(chan string, 1)
	lineCh := make(chan string, 1)
	go r.readLines(lineCh)
	go r.sendLines(lineCh)

	return r
}

func (r *lineReader) readLines(lineCh chan<- string) {
	defer close(lineCh)
	defer r.reader.Close()
	buf := bufio.NewReader(r.reader)
	for {
		line, err := buf.ReadString('\n')
		if err != nil && line == "" {
			return
		}

		select {
		case lineCh <- strings.TrimRight(line, "\n"):
		case <-r.ctx.Done():
			return
		}

		if err != nil {
			return
		}
	}
}

func (r *lineReader) sendLines(lineCh <-chan string) {
	var (
		open                   bool
		line                   string
		cuurentLine, sentLines uint
	)
	// When we're done, close the channel
	defer close(r.lines)

	for {
		cuurentLine++
		select {
		case line, open = <-lineCh:
			if !open {
				return
			}
		case <-time.After(r.timeout):
			return
		case <-r.ctx.Done():
			return
		}

		if cuurentLine < (r.offset + 1) {
			// skip this line
			continue
		}

		// Write out the line
		select {
		case r.lines <- line:
			sentLines++
		case <-r.ctx.Done():
			return
		}

		// If we hit the end, we're done
		if r.numLines > 0 && sentLines == r.numLines {
			return
		}
	}
}
