OUT_DIR?=build
SOURCE_FILES?=./...
TEST_RUNS?=1
TEST_PATTERN?=.
TEST_OPTIONS?=
TEST_TIMEOUT?=15s

GOFLAGS=-mod=vendor
export GOFLAGS

.PHONY: all build-out-dir
all: deps fmt test lint; $(info Building all...) @

out_dir:
	@mkdir -p $(OUT_DIR)

#######################################
## Environment set up.
#######################################

.PHONY: init deps fmt test lint

init:
	mkdir -p bin
	curl -sfL https://install.goreleaser.com/github.com/golangci/golangci-lint.sh | sh -s -- -b ./bin

deps:
	go mod download
	go mod tidy
	go mod vendor

#######################################
## Tests, lint and format.
#######################################

fmt: ; $(info Running gofmt...) @
	go fmt ./...

test: out_dir ; $(info Running unit tests...) @
	go test$(TEST_OPTIONS) -v -failfast -race -count=$(TEST_RUNS) \
	    -coverpkg=$(SOURCE_FILES) -covermode=atomic -coverprofile=$(OUT_DIR)/coverage.out $(SOURCE_FILES) \
	    -run $(TEST_PATTERN) -timeout=$(TEST_TIMEOUT)
	go tool cover -func=$(OUT_DIR)/coverage.out

#
# lint go code critical checks
#
# Shortcut for lint for developer

lint: out_dir
	$(if $(CI), make ci_lint, make lint_local)

lint_local: init
	./bin/golangci-lint run --fix -v

# Running lint with additional checks for sonarscanner
# Used by CI
ci_lint:
	golangci-lint run --out-format checkstyle > $(OUT_DIR)/lint-report.xml

